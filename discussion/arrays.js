
// SECTION - basic array structure
let bootcamp = [
	'Learn HTML', 
	'Use CSS',
	'Understand JS',
	'Maintain MongoDB',
	'Create components using React'
];

// SECTION - Access elements inside an array.
// How can we identify the index of an element inside an array?

console.log(bootcamp);
// container/array[indexNumber];

console.log(bootcamp[0]); // learn HTML
console.log(bootcamp[4]); // last element in this example

console.log(bootcamp[6]); // if we exceed number of index we will have an undefined return.

// SECTION - getting the length of an array structure.
// you can have access to the length property similar to what we do with strings.

console.log(bootcamp.length);
// this is useful for executing code that will depend on the number of contents/elements inside the storage.

// if (bootcamp.length > 5) {
// 	console.log('This is how long the array is, Do not exceed');
// }

// SUB SECTION - How to access the last element of an array.

// since the index of an element in an array starts with zero[0], we have to substract a value of one to the length of the array.

console.log(bootcamp.length - 1);
// the element that will be access is the last one in the inside the collection.

console.log(bootcamp[bootcamp.length - 1]);
// the element will be access instead of the number of index.

console.log('Batch 145');


// SECTION - Array Manipulators

// SUB-SECTION - Mutators

let bootcampTask = [];


// push() will add an element at the end of the array.
bootcampTask.push('Learn Javascript');
bootcampTask.push('Building a server using node');
bootcampTask.push('Utilizing express to build a server');


// unshift() add one or more elements at the 'FRONT' of the array
bootcampTask.unshift('Understand the concept of REST API', 'How to use Postman', 'Learn how to use MongoDB');


// shift() function removes the first element in the array and the value of the remove element can be assigned to a variable.
let akoNatanggalDahilKayShift = bootcampTask.shift()
console.log('This is the element removed by shift: ' + akoNatanggalDahilKayShift);


// pop() remove the last element of the array.
let elementRemovedUsingPop = bootcampTask.pop();
console.log('This is the element removed by pop: ' + elementRemovedUsingPop);


// splice() function can remove a specified number elements starting on a given index.
// syntax: arrayName.splice([startPosition], [#ofElementstoRemove], OPTIONAL [elementsToBeAdded])

// I want to remove all elements inside the array
// identify where the extraction will begin
// bootcampTask.splice(0, bootcampTask.length - 1);

// This syntax will not work since they only identify it or reference the correct index.
// bootcampTask.splice('How to use Postman', 'Learn Javascript');

bootcampTask.splice(1, 2, 'Learn Wireframing', 'Learn React');
// additional elements will be added to the front of the array.

console.log(bootcampTask);

// sort() - function re-arrange the elements in alphanumeric order.

let library = ['Pride and Prejudice', 'The Alchemist', 'Diary of a Pulubi', 'Beauty and the Beast'];

let series = [9,8,7,6,5];

series.sort();
console.log(series);

library.sort();
console.log(library);


// reverse() function reverses the order of the elements in an array.
// it will only reverse the element

let example1 = [9,8,7,6,5,15,89,27,36];

example1.reverse();
console.log(example1);
// output: [36, 27, 89, 15, 5, 6, 7, 8, 9]


// SUB-SECTION - Accessors
// indexOf() function finds/identify the index of a given element where it is first found.

			   // 0    1     2    3     4     5     6     7 
let countries = ['US','PH','CAN','SG','CAN','JAP','HON','CAN'];

// What if you want to targeta specific element in order to get its index number.
// in case of duplicate values, it will return the first instance of the value

let indexCount = countries.indexOf('CAN') //2
console.log('it is located at index: ' + indexCount);

// lastIndexOf() function finds the index of a given element where it is last found

let indexCount1 = countries.lastIndexOf('CAN'); //7
console.log('it is located at index: ' + indexCount1);


// SUB-SECTION - Iterators

// forEach() function is similar to a for loop that iterates on each array element

// what if you want to retrieve each element inside an array
// display all values in the array inside the console

// the function inside the forEach is called an anonymous function. These are functions that are defined and executed once only.

// syntax: arrayName.forEach(function(){WHAT TO DO TO EACH ELEMENT})

// we are going to pass a paremeter inside the function that will describe each single element inside the array.
bootcamp.forEach(function(task){
	// display each element individually inside the console.
	console.log(task);
});

// map() function iterates each elements and returns new array with different values depending on the result of the function's operation

let numbers = [1, 6, 7, 8, 9];

// pass an argument inside the function to identify which element inside the array
// numbers.map(function(num) {
// 	// this will tell the function what to do for each element.
// 	// only return the values in the series that will be the result of each number multiplied by itself.
// 	console.log(num * num);
// })

// every() function check if all elements passes a given condition.

let allValid = numbers.every(function(numero) {
	// we can specify the return
	return (numero <= 5);
})

console.log(allValid);


// some() - atleast 1 should pass the condition

let isGreaterThan7 = numbers.some(function(num) {
	// what to do
	return (num >= 7);
})

console.log('Did atleast 1 element passed?: ' + isGreaterThan7);

// filter() - function creates a new array that contains elements which passed a given condition.

let money = [1200, 2400, 6300, 990, 248];

let newMoneyStorage = money.filter(function(pera){
	return (pera >= 2000)
})
console.log(money); // the orig array is untouched
console.log(newMoneyStorage); // new container created function

// reduce() function evaluates elements from left to right and returns a single value

let newMoney = money.reduce(function(initialElement, nextElement) {
	return initialElement + nextElement;
})

console.log(newMoney)


let currency = [2, 1, 'Peso', 'Dollar', 'Yen', 'Dong', 'Ringgit'];

let addMoney = currency.reduce( function(left, right) {
	return left + right;
})

console.log(addMoney);


let students = ["Alice", "Jason", "Mark"];
console.log(students[students.length]);