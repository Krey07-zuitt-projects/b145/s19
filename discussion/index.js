console.log("Hello array");

// create a location/storage where the friends list will be stored.
let friendList = []; // non-persistent
console.log(friendList);

let res = document.getElementById('responseMessage');
let haba = friendList.length;

// SECTION - Mutator Functions

// Create a function to add new friend
function addNewFriend() {
	// get the data from the user
	let friendName = document.getElementById('friendName').value;
	// alert(friendName); // check only

	// Validate information by creating a control structure that will make sure that the input is not empty
	if (friendName !== '') {
		// proceed with the function
		// we will use the push() function
		friendList.push(friendName);
		console.log(friendList);
	} else {
		// failed
		res.innerHTML = '<h5 class="mb-1 mt-5 text-danger">Input is Invalid</h5>';
	}
}


// SECTION - Iteration Functions

// Create a function to check how many friends

function viewListLength() {
	// we will assess the length of  the array and inform the user how many elements are there in the array.
	if (haba === 0) {
		// inform the user that the container is empty
		res.innerHTML = 'You currently have 0 friends';
	} else {
		// display how many  friends
		res.innerHTML = 'You have ' + haba + ' friends';
	}
}